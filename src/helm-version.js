#!/usr/bin/env node

import yaml from 'js-yaml';
import fs from 'fs';
// import regex from 'regex';

export const helmVersion = (chartPath, version) => {
  const helmChart = yaml.safeLoad(fs.readFileSync(`${chartPath}/Chart.yaml`, 'utf8'));

  let versionArray = helmChart.version.split('.');
  if (version) {
    const versionRegexp = new RegExp(/[0-9]+.[0-9]+.[0-9]+/);
    switch (version) {
      case 'major':
        versionArray[0] = String(Number(versionArray[0]) + 1);
        break;
      case 'minor':
        versionArray[1] = String(Number(versionArray[1]) + 1);
        break;
      case 'patch':
        versionArray[2] = String(Number(versionArray[2]) + 1);
        break;
      default:
        if (versionRegexp.test(version)) versionArray = version.split('.');
        else throw new Error(`Incorrect version type "${version}".`);
    }
  }
  const versionString = versionArray.join('.');
  helmChart.version = versionString;
  helmChart.appVersion = versionString;
  fs.writeFileSync(`${chartPath}/Chart.yaml`, yaml.safeDump(helmChart));
  return versionString;
};

if (require.main === module) {
  if (process.argv.length < 3) {
    console.log('Usage:');
    console.log('helm-version <chart> [<newVersion> | major | minor | patch]\n');
    process.exit(0);
  }
  console.log(helmVersion(process.argv[2], process.argv[3]));
}
