# helm-version-js

Similar to npm version but for helm-charts, making it possible to bump versions of a helm-chart.

## Installation

```
npm install helm-version-js
```

## CLI

To use from CLI, install helm-version-js globally:

```
npm install -g helm-version-js
```

### Usage

```
helm-version <chart> [<newVersion> | major | minor | patch]
```

#### Examples:

```
helm-version path/to/helm-chart patch
helm-version path/to/helm-chart 3.32.1
```

## API

```
const { helmVersion } = require('helm-version);

helmVersion('path/to/helm-chart', 'patch');
helmVersion('path/to/helm-chart', '3.32.1');
```
