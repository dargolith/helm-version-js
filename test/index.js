import test from 'blue-tape';
import yaml from 'js-yaml';
import fs from 'fs';
import rimraf from 'rimraf';
import { helmVersion } from '../src/helm-version';

const testChart = {
  apiVersion: 'v1',
  appVersion: '1.20.300',
  description: 'Test chart',
  name: 'test',
  version: '1.20.300',
};

const initChart = () => {
  const dir = 'test/helm-chart';
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  const yamlString = yaml.safeDump(testChart);
  fs.writeFileSync('test/helm-chart/Chart.yaml', yamlString);
};

test('retrieve version', async t => {
  initChart();
  const expectedVersion = '1.20.300';
  const version = helmVersion('test/helm-chart');
  t.equals(version, expectedVersion);
});

test('major', async t => {
  initChart();
  const expectedVersion = '2.20.300';
  const version = helmVersion('test/helm-chart', 'major');
  t.equals(version, expectedVersion);
  const yamlChart = yaml.safeLoad(fs.readFileSync('test/helm-chart/Chart.yaml', 'utf8'));
  t.same(yamlChart, { ...testChart, appVersion: expectedVersion, version: expectedVersion });
});

test('minor', async t => {
  initChart();
  const expectedVersion = '1.21.300';
  const version = helmVersion('test/helm-chart', 'minor');
  t.equals(version, expectedVersion);
  const yamlChart = yaml.safeLoad(fs.readFileSync('test/helm-chart/Chart.yaml', 'utf8'));
  t.same(yamlChart, { ...testChart, appVersion: expectedVersion, version: expectedVersion });
});

test('patch', async t => {
  initChart();
  const expectedVersion = '1.20.301';
  const version = helmVersion('test/helm-chart', 'patch');
  t.equals(version, expectedVersion);
  const yamlChart = yaml.safeLoad(fs.readFileSync('test/helm-chart/Chart.yaml', 'utf8'));
  t.same(yamlChart, { ...testChart, appVersion: expectedVersion, version: expectedVersion });
});

test('explicit', async t => {
  initChart();
  const expectedVersion = '2.30.400';
  const version = helmVersion('test/helm-chart', expectedVersion);
  t.equals(version, expectedVersion);
  const yamlChart = yaml.safeLoad(fs.readFileSync('test/helm-chart/Chart.yaml', 'utf8'));
  t.same(yamlChart, { ...testChart, appVersion: expectedVersion, version: expectedVersion });
});

test('Cleanup test folder', async () => {
  rimraf.sync('test/helm-chart');
});
